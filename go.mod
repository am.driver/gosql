module gitlab.com/am.driver/gosql

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/xlab/closer v0.0.0-20190328110542-03326addb7c2 // indirect
)
